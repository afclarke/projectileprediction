﻿using System;
using UnityEngine;

public struct HitBallData
{
    public HitBallData(GameObject _ball, Vector3 _velocityChange)
    {
        ball = _ball;
        velocityChange = _velocityChange;
    }

    public GameObject ball;
    public Vector3 velocityChange;
}

public class Player : MonoBehaviour
{
    private CharacterController characterController;
    private GameObject ballInRange = null;
    private Animator animator;

    private bool swinging = false;

    public float moveSpeed = 4.0f;

    public static event EventHandler<HitBallData> HitBall;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (swinging) return;

        // move
        Vector3 moveDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveDir.Normalize();
        characterController.SimpleMove(moveDir * moveSpeed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Swing(moveDir);
        }
    }

    private void Swing(Vector3 dir)
    {
        swinging = true;
        if (ballInRange)
        {
            Swing swing = new Swing(dir.x, dir.z);
            Rigidbody ballRb = ballInRange.GetComponent<Rigidbody>();
            ballRb.velocity = Vector3.zero;
            Vector3 velocityChange = swing.dir * swing.vel;
            ballRb.AddForce(velocityChange, ForceMode.VelocityChange);
            HitBall?.Invoke(this, new HitBallData(ballInRange, velocityChange));
            ballInRange = null;
        }
        animator.Play("SWING");
    }

    public void OnSwingAnimEnd()
    {
        swinging = false;
    }

    public void BallInRange(GameObject ballOptional)
    {
        ballInRange = ballOptional;
    }
}
