﻿using System;
using UnityEngine;

public class PlayerSwingBox : MonoBehaviour
{
    private Player player;
    private void Start()
    {
        player = GetComponentInParent<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BALL"))
        {
            player.BallInRange(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("BALL"))
        {
            player.BallInRange(null);
        }
    }
}
