﻿using UnityEngine;

public struct Swing
{
    public Swing(float horizontal, float vertical)
    {
        horizontal = Mathf.Round(horizontal);
        vertical = Mathf.Round(vertical);

        dir = new Vector3(0, 0.5f, 1);
        if (horizontal >= 1)
        {
            dir.x = 0.2f;
        }
        else if (horizontal <= -1)
        {
            dir.x = -0.2f;
        }

        if (vertical >= 1)
        {
            vel = 12;
        }
        else if (vertical <= -1)
        {
            vel = 10;
        }
        else
        {
            vel = 11;
        }
    }

    public Vector3 dir;
    public float vel;
}
