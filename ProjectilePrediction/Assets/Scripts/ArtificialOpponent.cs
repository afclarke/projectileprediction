﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using Random = System.Random;

public class ArtificialOpponent : MonoBehaviour
{
    private CharacterController characterController;
    private Animator animator;

    private Vector3 steerTarget;
    private bool swinging = false;

    private Scene simScene;
    private PhysicsScene simPhysicsScene;

    public float moveSpeed = 4.0f;
    public float steerArriveMargin = 0.5f;
    public Transform baselineTransform;
    public Transform netTransform;

    public float simulationTimestep = 1.0f;
    public float simIterationsPerFrame = 5f;
    public GameObject court;
    public GameObject walls;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        Player.HitBall += (s, hitBallData) => { TrackBall(hitBallData); };
        steerTarget = baselineTransform.position;

        // create physics scene and instantiate copy of court to it
        CreateSceneParameters csp = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
        simScene = SceneManager.CreateScene("PhysicsSimulation", csp);
        simPhysicsScene = simScene.GetPhysicsScene();
        GameObject courtCopy = Instantiate(court);
        GameObject wallsCopy = Instantiate(walls);
        DisableRenderers(courtCopy);
        DisableRenderers(wallsCopy);
        SceneManager.MoveGameObjectToScene(courtCopy, simScene);
        SceneManager.MoveGameObjectToScene(wallsCopy, simScene);
    }

    private void Update()
    {
        if (swinging) return;

        Vector3 position = transform.position;
        steerTarget.y = position.y;
        Vector3 moveDir = steerTarget - position;
        if (moveDir.sqrMagnitude > steerArriveMargin * steerArriveMargin)
        {
            moveDir.Normalize();
            characterController.SimpleMove(moveDir * moveSpeed);
            Debug.DrawLine(position, steerTarget, Color.yellow, 0.0f);
        }
    }

    public void HitBall(GameObject ball)
    {
        // hit ball in random dir
        Swing swing = new Swing(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f));
        swing.dir.z *= -1;
        Rigidbody ballRb = ball.GetComponent<Rigidbody>();
        ballRb.velocity = Vector3.zero;
        ballRb.AddForce(swing.dir * swing.vel, ForceMode.VelocityChange);

        // anim & freeze
        Swing();

        // steer to closest rest point
        Vector3 pos = transform.position;
        float baselineDist = (baselineTransform.position - pos).sqrMagnitude;
        float netDist = (netTransform.position - pos).sqrMagnitude;
        steerTarget = baselineDist < netDist ? baselineTransform.position : netTransform.position;
    }

    private void Swing()
    {
        swinging = true;
        animator.Play("SWING");
    }

    public void OnSwingAnimEnd()
    {
        swinging = false;
    }

    private void TrackBall(HitBallData hitBallData)
    {
        StartCoroutine(SimulateTarget(hitBallData));
    }

    private IEnumerator SimulateTarget(HitBallData hitBallData)
    {
        GameObject ballCopy = Instantiate(hitBallData.ball);
        DisableRenderers(ballCopy);
        SceneManager.MoveGameObjectToScene(ballCopy, simScene);
        ballCopy.GetComponent<Rigidbody>().AddForce(hitBallData.velocityChange, ForceMode.VelocityChange);

        Vector3 pos = transform.position;

        int ballBounces = 0;
        ballCopy.GetComponent<Ball>().Bounce += (s, e) => { ballBounces++; };
        Transform ballCopyTransform = ballCopy.transform;

        Vector3 prevBallPos = ballCopyTransform.position;
        Vector3 closestBallPos = prevBallPos;
        float closestDist = (closestBallPos - pos).sqrMagnitude;

        int numIterations = 0;
        int numFrames = 1;
        while (ballBounces < 2)
        {
            if (numIterations > simIterationsPerFrame)
            {
                numFrames++;
                numIterations = 0;
                yield return 0;
            }
            numIterations++;

            simPhysicsScene.Simulate(simulationTimestep);
            Vector3 ballCopyPos = ballCopyTransform.position;

            Debug.DrawLine(prevBallPos, ballCopyPos, Color.red, 3.0f);
            prevBallPos = ballCopyPos;

            // out of range
            if (ballCopyPos.z < 0f || ballCopyPos.y < 0.5f || ballCopyPos.y > 2f) continue;

            float dist = (ballCopyPos - pos).sqrMagnitude;
            if (dist < closestDist)
            {
                closestDist = dist;
                closestBallPos = ballCopyPos;
            }
        }

        Destroy(ballCopy);

        Debug.Log("FRAMES: " + numFrames);

        // valid value
        if (closestBallPos.z > 0f)
        {
            steerTarget = closestBallPos;
        }
    }

    private static void DisableRenderers(GameObject gameObject)
    {
        var renderer = gameObject.GetComponent<Renderer>();
        if (renderer)
        {
            renderer.enabled = false;
        }
        foreach (var childRenderer in gameObject.GetComponentsInChildren<Renderer>())
        {
            if (childRenderer)
            {
                childRenderer.enabled = false;
            }
        }
    }
}
