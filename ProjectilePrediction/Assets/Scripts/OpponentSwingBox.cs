﻿using System;
using UnityEngine;

public class OpponentSwingBox : MonoBehaviour
{
    private ArtificialOpponent opponent;
    private void Start()
    {
        opponent = GetComponentInParent<ArtificialOpponent>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BALL"))
        {
            opponent.HitBall(other.gameObject);
        }
    }
}
