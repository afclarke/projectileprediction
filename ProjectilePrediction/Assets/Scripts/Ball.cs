﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public event EventHandler Bounce;

    private void OnCollisionEnter(Collision other)
    {
        Bounce?.Invoke(this, null);
    }
}
